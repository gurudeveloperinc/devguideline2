#  Gurudeveloper Dev Team Documentation 

### We love good coding

## For Web application development with `Laravel php`


**Visit the following links for:**

>[Prologue](https://laravel.com/docs/5.6/releases)


>[Getting Started](https://laravel.com/docs/5.6/installation)

>[Architecural Concept](https://laravel.com/docs/5.6/lifecycle)

## The Basics




## `Route` Definitions


```php

 Route::get('admin','HomeController@getAdmin');

```
`admin` - is the endpoints which is always displayed at the browser. 
`HomeController` - Defines the name of the controller.
`getAdmin` defines the name of the function.

>### Note:  The naming convention for writing `endpoints` and `functions` are to be writen in `camelCasing` format. Which is the general standard adopted by [Guru Dev Team](https://gurudeveloperinc.com/about/)

## `DATABASE` Naming Convention 

**.env file Configuration**

>The configuration of database is found in the .env file



```php
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=databasename
        DB_USERNAME=root
        DB_PASSWORD=
```

>DB_DATABASE - which is where the datbase name is defined should all be writen in `lowercase` Format

>[Details on env. configuration](http//:www.laravel.com) - work on it.

**Migration file Configuration**

>The Migration file is comprises of two main functions which includes: `function up()` and `function down()`

>The `Function up(){}` carries database schema that creates tables in the database. 

>While `function down(){}` carries database schema that deletes migrated tables from the database. 

**Naming Convention**
```php
            Schema::create( 'users', function ( Blueprint $table ) {
		    $table->increments( 'uid' );
		    $table->enum( 'role', [ 'Admin', 'Staff', 'Pharmacy', 'Lab', 'Hospital' ] );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'password', 60 );
		    $table->string( 'phone',191 )->unique();
		    $table->rememberToken();
		    $table->string( 'image', 2000 );
		    $table->string( 'address', 2000 );
		    $table->timestamps();
	    } );
```

> `users` - defines the name of the table. and should all be written in lowercase 

>the `increments` attributes defines the primary key and should also be written in lowercase. 

**Method of Definition**
>It is defined based on the first index of the table's schema name. Such as;

```php
//table name users
Schema::create( 'users', function ( Blueprint $table ) {
	    } );
 
```
```php
//primary key is prefixed with the first index of the table name 'u' and generally suffixed with 'id'
		    $table->increments( 'uid' );
		 
```

>If the table name is made up of two words such as doctor specializations, it should be writen with an underscore such as; 

```php
//table name doctor specialization
Schema::create( 'doctor_specialization', function ( Blueprint $table ) {
	    } );
 
```
```php
/*primary key should be prefixed with the first index of the two words that forms table name 'd' and 's' and generally suffixed with 'id'*/

		    $table->increments( 'dsid' );
		 
```

**Attributes with two words should be written in camelCase** Such as; 

```php
		    $table->string( 'hospitalAffiliation' );

//Or this if it has three words
		    $table->string( 'countryOfPractice' ); 
```

**For Enumerations**

>The attributes should be in this format
 
 ```php
		  $table->enum( 'gender', [ 'Male', 'Female' ] );
 ```

 ## `Function down()` 
 >Allows your to alter how migrations in the database
 
 
 >Works with the [table schema]() naming format for tables

>**DateTime Definition Format**


#[Eloquent ORM] (https://laravel.com/docs/5.6/eloquent)

>The Eloquent ORM included with Laravel provides a beautiful, simple ActiveRecord implementation for working with your database. Each database table has a corresponding "Model" which is used to interact with that table. Models allow you to query for data in your tables, as well as insert new records into the table.

>Before getting started, be sure to configure a database connection in config/database.php. For more information on configuring your database, check out laravel
[documentation](https://laravel.com/docs/5.6/database#configuration).

## Defining a model
Models can be created with migrations or without migration

```php
//To create model with a migration
php artisan make:model modelname --migration or -m

//To create model without migration
php artisan make:model modelname

```

# Model Convention


```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //definition of table name and primary key to the table name.
    protected $primaryKey = 'pid';
    protected $table      = 'patients';

}

```

**Data can be queried from a table in the database such as `patients` through the model. Check out laravel [documentation](https://laravel.com/docs/5.6/eloquent#inserting-and-updating-models)**

# Relationships 

>## One-to-One

A one-to-one relationship is a very basic relation. For example, a Patient model might be associated with one Phone. To define this relationship, we place a phone method on the Patient model. The phone method should call the hasOne method and return its `result:`

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function phone()
    {
        return $this->hasOne('App\Patient');
    }
}

```
>
 **Defining The Inverse Of The Relationship**

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * Get the user that owns the phone.
     */
    public function Patient()
    {
        return $this->belongsTo('App\Patient');
    }
}
```

>## One-to-Many

A `"one-to-many"` relationship is used to define relationships where a single model owns any amount of other models. For example, a paitent may have an infinite number of cases. Like all other Eloquent relationships, one-to-many relationships are defined by placing a function on your `Eloquent model:`

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patient extends Model
{
    protected $primaryKey = 'patid';
    protected $table = 'patients';

    public function Cases() {
        return $this->hasMany(cases::class,'patid','patid');
    }
}


```
 **Defining The Inverse Of The Relationship**

```php

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cases extends Model
{
    protected $primaryKey = 'casid';
    protected $table = 'cases';

    public function Patient() {
        return $this->belongsTo(patient::class,'patid','patid');
    }


}

```

>## Many-to-Many
 **Defining The Inverse Of The Relationship**




#Controller Convention 

**For `get` request**
```
public function getPharmacies(  ) {

    	$pharmacies = pharmacy::all();
    	return view('admin.pharmacy',[
    		'pharmacies' => $pharmacies
	    ]);
    }
```
>`Remember`: 
>All the function names are written in camelCasing 
**For a `post` request**

```php 
public function postCreatePatient(Request $request) {
		$patient = new patient();
		$patient->fname = $request->input('fname');
		$patient->sname = $request->input('sname');
		$patient->address = $request->input('address');
		$patient->phone = $request->input('phone');
		$patient->email = $request->input('email');
		$patient->uid = Auth::user()->uid;
		$status= $patient->save();
  
		if($status)
			$request->session()->flash('success','Client successfully created');
		else
			$request->session()->flash('error','Something went wrong please try again');

		return redirect('foldername/create-patient');

	}

```

#Views Conventions:

>All views are created per entities per a folder. 
> A single entity by defaults contains;

#Header file
**Note:-**
>Always include a head.blade in the layouts file and let it have the 
title and meta tags. Then reference it in the head section of app.blade like below
 
 ```php
 <head>
 
     @include('head')
 
 </head>
 ```
[entity folder]
`Manage`
`Edit`
`Add`
`Details`